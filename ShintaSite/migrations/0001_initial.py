# Generated by Django 2.2.5 on 2019-10-03 11:41

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AktivitasShinta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('waktu_mulai', models.DateTimeField(verbose_name='Start Time')),
                ('waktu_selesai', models.DateTimeField(verbose_name='End Time')),
                ('pesan', models.CharField(max_length=100)),
            ],
        ),
    ]
